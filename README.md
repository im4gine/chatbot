# React Native Chatbot

This is a fork of the project you can find on https://github.com/LucasBassetti/react-native-chatbot


## Usage

``` javascript
import ChatBot from 'react-native-chatbot';

const steps = [
  {
    id: '0',
    message: 'Welcome to react chatbot!',
    trigger: '1',
  },
  {
    id: '1',
    message: 'Bye!',
    end: true,
  },
];

<ChatBot steps={steps} />
```
