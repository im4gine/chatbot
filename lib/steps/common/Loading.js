import React from 'react';
import LoadingStep from './LoadingStep';

const Loading = () => (
  <LoadingStep className="rsc-loading">
    ...
  </LoadingStep>
);

export default Loading;
