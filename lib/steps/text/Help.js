import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'

export default class Help extends React.Component {
  render () {
    return (
      <TouchableOpacity onPress={this.props.helpAction}
          style={{
        borderWidth: 0.5,
        borderColor: 'grey',
        borderRadius: 20,
        padding: 10,
        width: 60,
        marginLeft: 10,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 5
      }}>
        <Text style={{color: 'grey', alignSelf: 'center'}}>Help</Text>
      </TouchableOpacity>
    )
  }
}
