import React, { Component } from 'react'
import { Dimensions, View } from 'react-native'
import PropTypes from 'prop-types'
import _ from 'lodash'
import Option from './Option'
import OptionElement from './OptionElement'
import OptionText from './OptionText'
import Options from './Options'

const {width} = Dimensions.get('window')
var createGroupedArray = function(arr, chunkSize) {
    var groups = [], i;
    for (i = 0; i < arr.length; i += chunkSize) {
        groups.push(arr.slice(i, i + chunkSize));
    }
    return groups;
}


class OptionsStep extends Component {
    /* istanbul ignore next */
    constructor (props) {
        super(props)

        this.renderOption = this.renderOption.bind(this)
        this.onOptionClick = this.onOptionClick.bind(this)
    }

    onOptionClick ({value}) {
        this.props.triggerNextStep({value})
    }

    renderOption (option) {
        const {bubbleStyle} = this.props
        const {bubbleColor, fontColor} = this.props.step
        const {value, label} = option

        return (
            <Option
                key={value}
                className="rsc-os-option"
                onPress={() => this.onOptionClick({value})}
            >
              <OptionElement
                  style={bubbleStyle}
                  bubbleColor={'#278BA6'}
              >
                <OptionText
                    class="rsc-os-option-text"
                    fontColor={'white'}
                >
                    {label}
                </OptionText>
              </OptionElement>
            </Option>
        )
    }

    renderOptions(options, renderOption){
        let views = [];
        let groupedArr = createGroupedArray(options, 2);
        groupedArr.forEach((array, key)=>{
            views.push(<View style={{flexDirection: 'row'}} key={key}>
                {_.map(array, this.renderOption)}
            </View>)
        })
        return views;
    }

    render () {
        const {options} = this.props.step

        return (
            <Options style={{width: width, alignItems: 'center', flexDirection: 'column'}}>
                {this.renderOptions(options, this.renderOption)}
            </Options>
        )
    }
}

OptionsStep.propTypes = {
    step: PropTypes.object.isRequired,
    triggerNextStep: PropTypes.func.isRequired,
    bubbleStyle: PropTypes.object.isRequired,
}

export default OptionsStep
